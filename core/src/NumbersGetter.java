import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anatoly on 12/16/2014.
 */
public class NumbersGetter {
    public static List<Integer> GetNumbers(String filePath) throws IOException {
        List<String> numbersStr = Files.readAllLines(Paths.get(filePath));
        List<Integer> numbers = new ArrayList<Integer>();
        for (String numberStr : numbersStr){
            numbers.add(Integer.valueOf(numberStr));
        }
        return numbers;
    }
}
