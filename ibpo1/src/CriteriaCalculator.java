import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anatoly on 12/19/2014.
 */
public class CriteriaCalculator {
    public static int K = 10;
    private static double Pj = 1.0 / K;

    public double calculate(List<Integer> numbers) {
        Integer max = Collections.max(numbers);
        Integer min = Collections.min(numbers);
        double sectionSize = Math.ceil(max / K);

        List<List<Integer>> sections = new ArrayList<List<Integer>>();
        for (int i = 0; i < K; i++) {
            sections.add(new ArrayList<Integer>());
        }

        for (Integer number : numbers) {
            double divided = number / sectionSize;
            int ceiled = (int) Math.ceil(divided);
            if (number.equals(0)) {
                ceiled = 1;
            }
            if (ceiled == 11) {
                ceiled = 10;
            }
            sections.get(ceiled - 1).add(number);
        }

        double criteria = criteria(sections, numbers.size());
        return criteria;
    }

    public double criteria(List<List<Integer>> sections, int numbersSize) {
        double resultSum = 0;
        for (int i = 0; i < K; i++) {
            List<Integer> section = sections.get(i);
            double result = calculateOne(numbersSize, section.size());
            resultSum += result;
        }
        return resultSum;
    }

    private double calculateOne(int numbersSize, int sectionSize) {
        int m = sectionSize;
        double v = numbersSize * Pj;
        double numerator = m - v;
        numerator = numerator * numerator;
        double v1 = numerator / v;
        return v1;
    }
}
