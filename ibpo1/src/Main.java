import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Anatoly
 * Date: 12/11/2014
 */
public class Main {

    public static double FREEDOM = CriteriaCalculator.K - 1;
    public static final double ALPHA = 0.95;

    public static void main(String[] args) throws IOException {
        List<List<Integer>> numbersLists = new ArrayList<List<Integer>>(){{
            add(NumbersGetter.GetNumbers("52/Task1/0"));
            add(NumbersGetter.GetNumbers("52/Task1/1"));
            add(NumbersGetter.GetNumbers("52/Task1/2"));
            add(NumbersGetter.GetNumbers("52/Task1/3"));
        }};
        List<Integer> numbersMsp = NumbersGetter.GetNumbers("samples/Task1/msp430");
        List<Integer> numbersSharp = NumbersGetter.GetNumbers("samples/Task1/Random_c#");
        List<Integer> numbersRng = NumbersGetter.GetNumbers("samples/Task1/RNGCrypto");
        List<Integer> numbersResid = NumbersGetter.GetNumbers("samples/Task1/Метод вычетов");
        CriteriaCalculator calculator = new CriteriaCalculator();

        double khi1 = calculator.calculate(numbersMsp);
        System.out.println("khi msp430: "+khi1);

        double khi2 = calculator.calculate(numbersSharp);
        System.out.println("khi Random_c#: "+khi2);

        double khi3 = calculator.calculate(numbersRng);
        System.out.println("khi RNGCrypto: "+khi3);

        double khi4 = calculator.calculate(numbersResid);
        System.out.println("khi Метод вычетов: "+khi4);

        for (int i = 0; i < numbersLists.size(); i++){
            double khi = calculator.calculate(numbersLists.get(i));
            System.out.println("khi for " + i + ": " + khi);
        }

        double khi = (1 - ALPHA) * FREEDOM;

        System.out.println("khi value: " + khi);

        //////////

        Second second = new Second();
        second.calculate(numbersLists.get(1));
    }
}
