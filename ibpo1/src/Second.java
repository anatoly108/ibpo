import java.util.Collections;
import java.util.List;

/**
 * Created by Anatoly on 12/19/2014.
 */
public class Second {
    public void calculate(List<Integer> numbers){
        Integer max = Collections.max(numbers);
        String maxBinary = Integer.toBinaryString(max);
        System.out.println(maxBinary);
    }
}
