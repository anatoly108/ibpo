import java.math.BigInteger;

/**
 * Created by Anatoly on 12/19/2014.
 */
public class Function {
    public BigInteger calculate(BigInteger a, BigInteger x, BigInteger c, BigInteger m){
        BigInteger v = a.multiply(x);
        BigInteger v1 = v.add(c);
        BigInteger a1 = v1.mod(m);
        return a1;
    }
}
