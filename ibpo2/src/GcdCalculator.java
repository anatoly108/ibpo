import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anatoly on 12/17/2014.
 */
public class GcdCalculator {
    public BigInteger calculate(ArrayList<BigInteger> x) {
        List<BigInteger> gcdList = new ArrayList<BigInteger>();
        BigInteger min = Collections.min(x);
        for (BigInteger number : x){
            BigInteger gcd = min.gcd(number);
            gcdList.add(gcd);
        }
        min = Collections.min(gcdList);
        return min;
    }
}
