import java.math.BigInteger;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Anatoly on 12/18/2014.
 */
public class Iterator extends Thread {
    private Logger logger;

    private BigInteger from;
    private BigInteger to;
    List<Integer> numbers;
    private String name;
    private SequenceCalculator sequenceCalculator;
    private BigInteger m;

    public Iterator(BigInteger from, BigInteger to, List<Integer> numbers, String name, Logger logger, SequenceCalculator sequenceCalculator, BigInteger m) {
        this.from = from;
        this.to = to;
        this.numbers = numbers;
        this.name = name;
        this.logger = logger;
        this.sequenceCalculator = sequenceCalculator;
        this.m = m;
    }

    @Override
    public void run() {
        logger.info("hello, i'm " + name);
        BigInteger a = from;
        BigInteger c = BigInteger.ZERO;

        boolean found = false;
        BigInteger integer = new BigInteger(""+numbers.get(0));
        while (!found) {
            c = c.add(BigInteger.ONE);
            if(c.equals(Main.MAXIMUM)){
                if(a.equals(to)){
                    break;
                }
                logger.info(name + ": on a = " + a);
                c = BigInteger.ONE;
                a = a.add(BigInteger.ONE);
            }
            BigInteger pastNumber = integer;
            found = sequenceCalculator.calculate(a, pastNumber, c, m, numbers);
        }
        if(found){
            logger.info(name + " FOUND THE ANSWER! c = " + c + " and a = " + a + " and m = " + m);
        } else {
            logger.info(name + " finished, no answer");
        }
    }
}
