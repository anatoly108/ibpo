import Jama.Matrix;

import java.math.BigInteger;
import java.util.*;

/**
 * Created by Anatoly on 12/17/2014.
 */
public class MCalculator {
    private Random random = new Random();
    private final int ITERATIONS_N = 100;
    private final int GLOBAL_ITERATIONS_N = 10000;
    private final int RANDOM_N = 4;
    private GcdCalculator gcdCalculator;

    public MCalculator(GcdCalculator gcdCalculator) {
        this.gcdCalculator = gcdCalculator;
    }

    public BigInteger calculate(List<Integer> numbers) {
        ArrayList<BigInteger> gcds = new ArrayList<BigInteger>();
        for (int j = 0; j < GLOBAL_ITERATIONS_N; j++) {
            ArrayList<BigInteger> determinants = new ArrayList<BigInteger>();
            for (int i = 0; i < ITERATIONS_N; i++) {
                Integer[] fr = getRandomNumbers(numbers);
                double[][] array = {{fr[0], fr[1], 1}, {fr[1], fr[2], 1}, {fr[2], fr[3], 1}};
                Matrix A = new Matrix(array);
                double determinant = A.det();
                long determinantLong = Math.round(determinant);
                determinants.add(BigInteger.valueOf(determinantLong));
            }
            BigInteger gdc = gcdCalculator.calculate(determinants);
            gcds.add(gdc);
        }
        BigInteger min = Collections.min(gcds);
        return min;
    }


    private int randInt(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

    private Integer[] getRandomNumbers(List<Integer> numbers) {
        Integer[] result = new Integer[RANDOM_N];
        int index = randInt(0, numbers.size() - RANDOM_N);
        for(int i = 0; i < RANDOM_N; i++){
            result[i] = numbers.get(index++);
        }
        return result;
    }

}
