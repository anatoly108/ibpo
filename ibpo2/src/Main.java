import java.io.IOException;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * User: Anatoly
 * Date: 12/11/2014
 */
public class Main {

    private static final int THREAD_NUM = 6;
    public static final BigInteger MAXIMUM = BigInteger.valueOf(1000000L);

    public static void main(String[] args) throws IOException {
        List<Integer> numbers = NumbersGetter.GetNumbers("52/Task2/1");

        int range = MAXIMUM.intValue() / THREAD_NUM;

        FileHandler fh = new FileHandler("log.log");
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);
        Logger logger = Logger.getLogger("name");
        logger.addHandler(fh);

        GcdCalculator gcdCalculator = new GcdCalculator();
        MCalculator mCalculator = new MCalculator(gcdCalculator);
        BigInteger m = mCalculator.calculate(numbers);
//        Integer max = Collections.max(numbers);
//        BigInteger m = new BigInteger(""+max);

        String[] names = new String[THREAD_NUM];
        names[0] = "John";
        names[1] = "Antuan";
        names[2] = "Geek";
        names[3] = "Garry";
        names[4] = "Fox";
        names[5] = "Goldmund";
        logger.info("range is " + range);
        for (int i = 0; i < THREAD_NUM; i++){
            BigInteger floor = new BigInteger(""+(i * range));
            BigInteger ceil = floor.add(new BigInteger(""+range));
            Function function = new Function();
            SequenceCalculator sequenceCalculator = new SequenceCalculator(function);
            Iterator iterator = new Iterator(floor, ceil, numbers, names[i], logger, sequenceCalculator, m);
            iterator.start();
        }
    }

}
