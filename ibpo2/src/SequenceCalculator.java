import java.math.BigInteger;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Anatoly on 12/19/2014.
 */
public class SequenceCalculator {
    private static int NUMBERS_TO_CHECK = 10;
    private Function function;

    public SequenceCalculator(Function function) {
        this.function = function;
    }

    public boolean calculate(BigInteger a, BigInteger pastNumber, BigInteger c, BigInteger m, List<Integer> numbers){
        BigInteger past = pastNumber;
        for (int i = 1; i < NUMBERS_TO_CHECK; i++) {
            BigInteger result = function.calculate(a, past, c, m);
            Integer numberToCompare = numbers.get(i);
            if(result.intValue() != numberToCompare){
                break;
            }
            past = result;

            if(i == NUMBERS_TO_CHECK - 1){

                return true;
            }
        }
        return false;
    }
}
