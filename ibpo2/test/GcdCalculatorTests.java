import org.junit.Assert;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by Anatoly on 12/17/2014.
 */
public class GcdCalculatorTests {
    @org.junit.Test
    public void testName() throws Exception {
        GcdCalculator target = new GcdCalculator();
        ArrayList<BigInteger> x = new ArrayList<BigInteger>();
        x.add(new BigInteger("24"));
        x.add(new BigInteger("8"));
        x.add(new BigInteger("3"));
        BigInteger actual = target.calculate(x);
        BigInteger expected = new BigInteger("1");
        Assert.assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testName2() throws Exception {
        GcdCalculator target = new GcdCalculator();
        ArrayList<BigInteger> x = new ArrayList<BigInteger>();
        x.add(new BigInteger("300"));
        x.add(new BigInteger("8"));
        x.add(new BigInteger("30"));
        BigInteger actual = target.calculate(x);
        BigInteger expected = new BigInteger("2");
        Assert.assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testName3() throws Exception {
        GcdCalculator target = new GcdCalculator();
        ArrayList<BigInteger> x = new ArrayList<BigInteger>();
        x.add(new BigInteger("24"));
        x.add(new BigInteger("3"));
        x.add(new BigInteger("27"));
        BigInteger actual = target.calculate(x);
        BigInteger expected = new BigInteger("3");
        Assert.assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testName4() throws Exception {
        GcdCalculator target = new GcdCalculator();
        ArrayList<BigInteger> x = new ArrayList<BigInteger>();
        x.add(new BigInteger("25"));
        x.add(new BigInteger("3"));
        x.add(new BigInteger("27"));
        BigInteger actual = target.calculate(x);
        BigInteger expected = new BigInteger("1");
        Assert.assertEquals(expected, actual);
    }
}
