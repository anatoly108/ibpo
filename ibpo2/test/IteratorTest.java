import org.junit.Test;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Anatoly on 12/18/2014.
 */
public class IteratorTest {
    @Test
    public void testName() throws Exception {
        BigInteger m = new BigInteger(""+1000000);
        BigInteger a = BigInteger.valueOf(525682L);
        BigInteger c = BigInteger.valueOf(977270L);

        List<Integer> numbers = NumbersGetter.GetNumbers("52/Task2/1");
        Function function = new Function();
        SequenceCalculator target = new SequenceCalculator(function);
        BigInteger integer = new BigInteger(""+numbers.get(0));
        boolean actual = target.calculate(a, integer, c, m, numbers);
        System.out.println(actual);
    }

    @Test
    public void testName2() throws Exception {
        BigInteger m = new BigInteger(""+998442);
        BigInteger a = BigInteger.valueOf(525682L);
        BigInteger c = BigInteger.valueOf(977270L);

        List<Integer> numbers = NumbersGetter.GetNumbers("52/Task2/1");
        Function function = new Function();
        SequenceCalculator target = new SequenceCalculator(function);
        BigInteger integer = new BigInteger(""+numbers.get(0));
        boolean actual = target.calculate(a, integer, c, m, numbers);
        System.out.println(actual);
    }
}
